Ejemplo de como utilizar GIT y algunos comandos 
por primera vez utilizar el comando git clone

git clone https://gitlab.com/eliasg.videla/ejemplogitcmd.git


Comandos de consulta
git status

Agregar uno o varios archivos al repositorio ("." es un comodin)
git add . (archivo)

Mensaje que aparece al consultar en el repositorio
git commit -am "Mensaje de lo realizado en el proyecto"
(el commit es el que te permite hacer el push, el push es el que confirma 
y sube los archivos al servidor)

Subir lo realizado al servidor
git push origin master (no es necesario poner origin master)

Esto lo hago cada vez que empiezo a trabajar cotidianamente
git pull origin master (para fijarse si alguno de mi equipo subi� algo)